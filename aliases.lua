--[[
More Blocks: alias definitions

Copyright (c) 2011-2015 Calinou and contributors.
Licensed under the zlib license. See LICENSE.md for more information.
--]]
-- acacia aliases
minetest.register_alias("moretrees:slab_acacia_planks", "moreblocks:slab_acacia_wood")
minetest.register_alias("moretrees:slab_acacia_planks_1", "moreblocks:slab_acacia_wood_1")
minetest.register_alias("moretrees:slab_acacia_planks_2", "moreblocks:slab_acacia_wood_2")
minetest.register_alias("moretrees:slab_acacia_planks_quarter", "moreblocks:slab_acacia_wood_quarter")
minetest.register_alias("moretrees:slab_acacia_planks_three_quarter", "moreblocks:slab_acacia_wood_three_quarter")
minetest.register_alias("moretrees:slab_acacia_planks_14", "moreblocks:slab_acacia_wood_14")
minetest.register_alias("moretrees:slab_acacia_planks_15", "moreblocks:slab_acacia_wood_15")

minetest.register_alias("moretrees:panel_acacia_planks", "moreblocks:panel_acacia_wood")
minetest.register_alias("moretrees:panel_acacia_planks_1", "moreblocks:panel_acacia_wood_1")
minetest.register_alias("moretrees:panel_acacia_planks_2", "moreblocks:panel_acacia_wood_2")
minetest.register_alias("moretrees:panel_acacia_planks_4", "moreblocks:panel_acacia_wood_4")
minetest.register_alias("moretrees:panel_acacia_planks_12", "moreblocks:panel_acacia_wood_12")
minetest.register_alias("moretrees:panel_acacia_planks_14", "moreblocks:panel_acacia_wood_14")
minetest.register_alias("moretrees:panel_acacia_planks_15", "moreblocks:panel_acacia_wood_15")

minetest.register_alias("moretrees:micro_acacia_planks", "moreblocks:micro_acacia_wood")
minetest.register_alias("moretrees:micro_acacia_planks_1", "moreblocks:micro_acacia_wood_1")
minetest.register_alias("moretrees:micro_acacia_planks_2", "moreblocks:micro_acacia_wood_2")
minetest.register_alias("moretrees:micro_acacia_planks_4", "moreblocks:micro_acacia_wood_4")
minetest.register_alias("moretrees:micro_acacia_planks_12", "moreblocks:micro_acacia_wood_12")
minetest.register_alias("moretrees:micro_acacia_planks_14", "moreblocks:micro_acacia_wood_14")
minetest.register_alias("moretrees:micro_acacia_planks_15", "moreblocks:micro_acacia_wood_15")

minetest.register_alias("moretrees:stair_acacia_planks", "moreblocks:stair_acacia_wood")
minetest.register_alias("moretrees:stair_acacia_planks_half", "moreblocks:stair_acacia_wood_half")
minetest.register_alias("moretrees:stair_acacia_planks_inner", "moreblocks:stair_acacia_wood_inner")
minetest.register_alias("moretrees:stair_acacia_planks_outer", "moreblocks:stair_acacia_wood_outer")
minetest.register_alias("moretrees:stair_acacia_planks_alt", "moreblocks:stair_acacia_wood_alt")
minetest.register_alias("moretrees:stair_acacia_planks_alt_1", "moreblocks:stair_acacia_wood_alt_1")
minetest.register_alias("moretrees:stair_acacia_planks_alt_2", "moreblocks:stair_acacia_wood_alt_2")
minetest.register_alias("moretrees:stair_acacia_planks_alt_4", "moreblocks:stair_acacia_wood_alt_4")

-- wool aliases
local colors_nodes = {
        "white",
        "grey",
        "black",
        "red",
        "yellow",
        "green",
        "cyan",
        "blue",
        "magenta",
        "orange",
        "violet",
        "brown",
        "pink",
        "dark_grey",
        "dark_green",
}
for _,color in pairs(colors_nodes) do

minetest.register_alias("moreblocks:slab_wool_" .. color, "wool:slab_" .. color)
minetest.register_alias("moreblocks:slab_wool_" .. color .. "_1", "wool:slab_" .. color .. "_1")
minetest.register_alias("moreblocks:slab_wool_" .. color .. "_2", "wool:slab_" .. color .. "_2")
minetest.register_alias("moreblocks:slab_wool_" .. color .. "_quarter", "wool:slab_" .. color .. "_quarter")
minetest.register_alias("moreblocks:slab_wool_" .. color .. "_three_quarter", "wool:slab_" .. color .. "_three_quarter")
minetest.register_alias("moreblocks:slab_wool_" .. color .. "_14", "wool:slab_" .. color .. "_14")
minetest.register_alias("moreblocks:slab_wool_" .. color .. "_15", "wool:slab_" .. color .. "_15")

minetest.register_alias("moreblocks:panel_wool_" .. color, "wool:panel_" .. color)
minetest.register_alias("moreblocks:panel_wool_" .. color .. "_1", "wool:panel_" .. color .. "_1")
minetest.register_alias("moreblocks:panel_wool_" .. color .. "_2", "wool:panel_" .. color .. "_2")
minetest.register_alias("moreblocks:panel_wool_" .. color .. "_4", "wool:panel_" .. color .. "_4")
minetest.register_alias("moreblocks:panel_wool_" .. color .. "_12", "wool:panel_" .. color .. "_12")
minetest.register_alias("moreblocks:panel_wool_" .. color .. "_14", "wool:panel_" .. color .. "_14")
minetest.register_alias("moreblocks:panel_wool_" .. color .. "_15", "wool:panel_" .. color .. "_15")

minetest.register_alias("moreblocks:micro_wool_" .. color, "wool:micro_" .. color)
minetest.register_alias("moreblocks:micro_wool_" .. color .. "_1", "wool:micro_" .. color .. "_1")
minetest.register_alias("moreblocks:micro_wool_" .. color .. "_2", "wool:micro_" .. color .. "_2")
minetest.register_alias("moreblocks:micro_wool_" .. color .. "_4", "wool:micro_" .. color .. "_4")
minetest.register_alias("moreblocks:micro_wool_" .. color .. "_12", "wool:micro_" .. color .. "_12")
minetest.register_alias("moreblocks:micro_wool_" .. color .. "_14", "wool:micro_" .. color .. "_14")
minetest.register_alias("moreblocks:micro_wool_" .. color .. "_15", "wool:micro_" .. color .. "_15")

minetest.register_alias("moreblocks:stair_wool_" .. color, "wool:stair_" .. color)
minetest.register_alias("moreblocks:stair_wool_" .. color .. "_half", "wool:stair_" .. color .. "_half")
minetest.register_alias("moreblocks:stair_wool_" .. color .. "_inner", "wool:stair_" .. color .. "_inner")
minetest.register_alias("moreblocks:stair_wool_" .. color .. "_outer", "wool:stair_" .. color .. "_outer")
minetest.register_alias("moreblocks:stair_wool_" .. color .. "_alt", "wool:stair_" .. color .. "_alt")
minetest.register_alias("moreblocks:stair_wool_" .. color .. "_alt_1", "wool:stair_" .. color .. "_alt_1")
minetest.register_alias("moreblocks:stair_wool_" .. color .. "_alt_2", "wool:stair_" .. color .. "_alt_2")
minetest.register_alias("moreblocks:stair_wool_" .. color .. "_alt_4", "wool:stair_" .. color .. "_alt_4")
end

-- bamboo aliases
local bamb_nodes = {
        "block",
        "block_dry",
}
for _,color in pairs(bamb_nodes) do

minetest.register_alias("moreblocks:slab_" .. color, "bamboo:slab_" .. color)
minetest.register_alias("moreblocks:slab_" .. color .. "_1", "bamboo:slab_" .. color .. "_1")
minetest.register_alias("moreblocks:slab_" .. color .. "_2", "bamboo:slab_" .. color .. "_2")
minetest.register_alias("moreblocks:slab_" .. color .. "_quarter", "bamboo:slab_" .. color .. "_quarter")
minetest.register_alias("moreblocks:slab_" .. color .. "_three_quarter", "bamboo:slab_" .. color .. "_three_quarter")
minetest.register_alias("moreblocks:slab_" .. color .. "_14", "bamboo:slab_" .. color .. "_14")
minetest.register_alias("moreblocks:slab_" .. color .. "_15", "bamboo:slab_" .. color .. "_15")

minetest.register_alias("moreblocks:panel_" .. color, "bamboo:panel_" .. color)
minetest.register_alias("moreblocks:panel_" .. color .. "_1", "bamboo:panel_" .. color .. "_1")
minetest.register_alias("moreblocks:panel_" .. color .. "_2", "bamboo:panel_" .. color .. "_2")
minetest.register_alias("moreblocks:panel_" .. color .. "_4", "bamboo:panel_" .. color .. "_4")
minetest.register_alias("moreblocks:panel_" .. color .. "_12", "bamboo:panel_" .. color .. "_12")
minetest.register_alias("moreblocks:panel_" .. color .. "_14", "bamboo:panel_" .. color .. "_14")
minetest.register_alias("moreblocks:panel_" .. color .. "_15", "bamboo:panel_" .. color .. "_15")

minetest.register_alias("moreblocks:micro_" .. color, "bamboo:micro_" .. color)
minetest.register_alias("moreblocks:micro_" .. color .. "_1", "bamboo:micro_" .. color .. "_1")
minetest.register_alias("moreblocks:micro_" .. color .. "_2", "bamboo:micro_" .. color .. "_2")
minetest.register_alias("moreblocks:micro_" .. color .. "_4", "bamboo:micro_" .. color .. "_4")
minetest.register_alias("moreblocks:micro_" .. color .. "_12", "bamboo:micro_" .. color .. "_12")
minetest.register_alias("moreblocks:micro_" .. color .. "_14", "bamboo:micro_" .. color .. "_14")
minetest.register_alias("moreblocks:micro_" .. color .. "_15", "bamboo:micro_" .. color .. "_15")

minetest.register_alias("moreblocks:stair_" .. color, "bamboo:stair_" .. color)
minetest.register_alias("moreblocks:stair_" .. color .. "_half", "bamboo:stair_" .. color .. "_half")
minetest.register_alias("moreblocks:stair_" .. color .. "_inner", "bamboo:stair_" .. color .. "_inner")
minetest.register_alias("moreblocks:stair_" .. color .. "_outer", "bamboo:stair_" .. color .. "_outer")
minetest.register_alias("moreblocks:stair_" .. color .. "_alt", "bamboo:stair_" .. color .. "_alt")
minetest.register_alias("moreblocks:stair_" .. color .. "_alt_1", "bamboo:stair_" .. color .. "_alt_1")
minetest.register_alias("moreblocks:stair_" .. color .. "_alt_2", "bamboo:stair_" .. color .. "_alt_2")
minetest.register_alias("moreblocks:stair_" .. color .. "_alt_4", "bamboo:stair_" .. color .. "_alt_4")
end

-- More Blocks aliases:
minetest.register_alias("sweeper", "moreblocks:sweeper")
minetest.register_alias("circular_saw", "moreblocks:circular_saw")
minetest.register_alias("jungle_stick", "moreblocks:jungle_stick")

-- Old block/item replacement:
minetest.register_alias("moreblocks:oerkkiblock", "default:mossycobble")
minetest.register_alias("moreblocks:screwdriver", "screwdriver:screwdriver")

-- Node and item renaming:
minetest.register_alias("moreblocks:stone_bricks", "default:stonebrick")
minetest.register_alias("moreblocks:stonebrick", "default:stonebrick")
minetest.register_alias("moreblocks:junglewood", "default:junglewood")
minetest.register_alias("moreblocks:jungle_wood", "default:junglewood")

for _, t in pairs(circular_saw.names) do
	minetest.register_alias("moreblocks:" .. t[1] .. "_jungle_wood" .. t[2],
			"moreblocks:" .. t[1] .. "_junglewood" .. t[2])
end
minetest.register_alias("moreblocks:horizontaltree", "moreblocks:horizontal_tree")
minetest.register_alias("moreblocks:horizontaljungletree", "moreblocks:horizontal_jungle_tree")
minetest.register_alias("moreblocks:stonesquare", "moreblocks:stone_tile")
minetest.register_alias("moreblocks:circlestonebrick", "moreblocks:circle_stone_bricks")
minetest.register_alias("moreblocks:ironstonebrick", "moreblocks:iron_stone_bricks")
minetest.register_alias("moreblocks:fence_junglewood", "moreblocks:fence_jungle_wood")
minetest.register_alias("moreblocks:coalstone", "moreblocks:coal_stone")
minetest.register_alias("moreblocks:ironstone", "moreblocks:iron_stone")
minetest.register_alias("moreblocks:woodtile", "moreblocks:wood_tile")
minetest.register_alias("moreblocks:woodtile_full", "moreblocks:wood_tile_full")
minetest.register_alias("moreblocks:woodtile_centered", "moreblocks:wood_tile_centered")
minetest.register_alias("moreblocks:woodtile_up", "moreblocks:wood_tile_up")
minetest.register_alias("moreblocks:woodtile_down", "moreblocks:wood_tile_down")
minetest.register_alias("moreblocks:woodtile_left", "moreblocks:wood_tile_left")
minetest.register_alias("moreblocks:woodtile_right", "moreblocks:wood_tile_right")
minetest.register_alias("moreblocks:coalglass", "moreblocks:coal_glass")
minetest.register_alias("moreblocks:ironglass", "moreblocks:iron_glass")
minetest.register_alias("moreblocks:glowglass", "moreblocks:glow_glass")
minetest.register_alias("moreblocks:superglowglass", "moreblocks:super_glow_glass")
minetest.register_alias("moreblocks:trapglass", "moreblocks:trap_glass")
minetest.register_alias("moreblocks:trapstone", "moreblocks:trap_stone")
minetest.register_alias("moreblocks:cactuschecker", "moreblocks:cactus_checker")
minetest.register_alias("moreblocks:coalchecker", "moreblocks:coal_checker")
minetest.register_alias("moreblocks:ironchecker", "moreblocks:iron_checker")
minetest.register_alias("moreblocks:cactusbrick", "moreblocks:cactus_brick")
minetest.register_alias("moreblocks:cleanglass", "moreblocks:clean_glass")
minetest.register_alias("moreblocks:emptybookshelf", "moreblocks:empty_bookshelf")
minetest.register_alias("moreblocks:junglestick", "moreblocks:jungle_stick")
minetest.register_alias("moreblocks:splitstonesquare","moreblocks:split_stone_tile")
minetest.register_alias("moreblocks:allfacestree","moreblocks:all_faces_tree")

-- ABM for horizontal trees (fix facedir):
local horizontal_tree_convert_facedir = {7, 12, 9, 18}

minetest.register_abm({
	nodenames = {"moreblocks:horizontal_tree","moreblocks:horizontal_jungle_tree"},
	interval = 1,
	chance = 1,
	action = function(pos, node)
		if node.name == "moreblocks:horizontal_tree" then
			node.name = "default:tree"
		else
			node.name = "default:jungletree"
		end
		node.param2 = node.param2 < 3 and node.param2 or 0
		minetest.set_node(pos, {
			name = node.name,
			param2 = horizontal_tree_convert_facedir[node.param2 + 1]
		})
	end,
})